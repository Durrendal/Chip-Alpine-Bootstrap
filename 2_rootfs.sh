#!/bin/ash
set -x
source common/utils.sh
source conf/variables.conf

#If you forgot to run prepare, we'll just go ahead and fix that for you
if [ ! -d $rootfs ] | [ ! -d $cache ]; then
	./1_prepare.sh
fi

#Copy our resolv to the rootfs
cp /etc/resolv.conf $rootfs/etc/

#Copy the arm-static binary if the cpu arch isn't armhf/vy or aarch64
if [ "$CARCH" != "armhf" ] | [ "$CARCH" != "armv7" ] | [ "$CARCH" != "aarch64" ]; then
	#Copy qemu static binary into chroot, and make it executable
	cp -a $cache/$arm_static $rootfs/usr/bin/
	chmod +x $rootfs/usr/bin/$arm_static

	#If binfmt isn't mounted, do so
	if [ ! -f /proc/sys/fs/binfmt_misc/register ]; then
		escalate mount binfmt_misc -t binfmt_misc /proc/sys/fs/binfmt_misc
	fi

	#Enable qemu-arm-static binary as our binfmt emulator
	escalate common/./qemu-binfmt-conf.sh --qemu-path /usr/bin

	#This method also works, it essentially mount binfmt_misc, and runs the qemu-binfmt-conf.sh, and provides the qemu-*-static binaries.
	#escalate docker run --rm --privileged multiarch/qemu-user-static:register --reset --credential yes
	
	chroot_cmd="$arm_static /bin/ash -l"
else
	chroot_cmd="/bin/ash -l"
fi

#Mount special file systems
escalate mount -t proc none $rootfs/proc
escalate mount -o bind /sys $rootfs/sys
escalate mount -o bind /dev $rootfs/dev

#Perform a baseline installation inside of the rootfs via chroot
cat <<EOF | escalate chroot $rootfs $chroot_cmd
set -x
#Set repositories
printf "$cdn/v$version/main\n$cdn/v$version/community\n" > /etc/apk/repositories

#Update the rootfs
apk update
apk -U -a upgrade

#Install base system packages
apk add alpine-base alpine-baselayout ca-certificates-bundle alpine-keys mkinitfs agetty wpa_supplicant wireless-tools openssh openssh-server chrony tzdata dhcpcd openrc dbus syslog-ng logrotate tmux mg htop udev-init-scripts eudev eudev-hwids hwids hwdata hwdata-usb binutils coreutils dateutils findutils pciutils usbutils util-linux-misc procps-ng e2fsprogs iproute2 gawk sed grep openrc

services=boot/bootmisc boot/modules boot/sysctl boot/syslog-ng boot/networking sysinit/devfs sysinit/hwdrivers sysinit/udev sysinit/udev-trigger sysinit/modules sysint/dmesg shutdown/mount-ro shutdown/killprocs default/dbus default/chronyd default/local default/sshd default/udev default/udev-postmount 

for rc in $services; do
	ln -s /etc/init.d/"${rc##*/}" /etc/runlevels/"$rc"
done

#Create home dir for root, and set a blank password
if [ ! -d /root ]; then mkdir -p /root; fi
passwd root -d

#Allow login over serial, these are usually configured by default
for tty in ttyS0 ttyGS0; do
	configured=$(grep -o ^${tty}$ /etc/securetty)
	if [ "$configured" !=  "$tty" ]; then
	   echo $tty >> /etc/securetty
	fi
done

#Enable virtual serial module, these may be configured already
for module in g_serial 8723bs; do
	configured=$(grep -o ^${module}$ /etc/modules)
	if [ "$configured" !=  "$module" ]; then
		echo $module >> /etc/modules
	fi
done

#Ensure TTY are spawned for serial devices, this is almost never configured by default
echo ttyS0::respawn:/sbin/getty -L ttyS0 115200 vt102 >> /etc/inittab
echo ttyGS0::respawn:/sbin/getty -L ttyGS0 115200 vt102 >> /etc/inittab

exit
EOF

#Deactivate the chroot file systems
#escalate umount $rootfs/proc
#escalate umount $rootfs/sys
#escalate umount $rootfs/dev

#Copy modules & kernel
cp -r $cache/lib/modules $rootfs/lib/modules
cp -a $cache/usr/lib/linux-image-${kernel_version} $rootfs/usr/lib/

#TODO:
#Custom Kernel Compilation!
