#!/bin/ash

#Usage: escalate apk add $to_add
escalate() {
	if [ "$(whoami)" != root ]; then
		if [ $(which sudo) ]; then
			sudo $@
		elif [ $(which doas) ]; then
			doas $@
		fi
	else
		$@
	fi
}

logo() {
	echo "   #  #  #"
	echo "  #########"
	echo "###       ###"
	echo "  # {#}   #"
	echo "###  '\######"
	echo "  #       #"
	echo "###       ###"
	echo "  ########"
	echo "   #  #  #"
}
