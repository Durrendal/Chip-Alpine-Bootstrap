#!/bin/ash
#set -x
source common/utils.sh
source conf/variables.conf
deps="binutils util-linux-misc coreutils sunxi-tools mtd-utils mtd-utils-flash mtd-utils-jffs mtd-utils-misc mtd-utils-nand mtd-utils-nor mtd-utils-ubi screen u-boot-tools android-tools eudev eudev-hwids shadow"
logo

#Check if this is an alpine system
if [ ! -f /etc/apk/world ]; then
	echo "You may need to modify this to run it on a non-Alpine distro. Or just switch over to an Alpine environment to build if that's easier."
	exit 1
fi

#Check if the build system is an armhf system, if not, add binutils to the deps
CARCH=$(uname -m)
if [ "$CARCH" != "armhf" ] | [ "$CARCH" != "armv7" ] | [ "$CARCH" != "aarch64" ]; then
	deps="$deps qemu-system-arm qemu-arm"
fi

#Iterate over deps, collect ones that need to be installed
to_add=""
for dep in $deps; do
	exists=$(grep ^$dep$ /etc/apk/world)
	if [ "$exists" != "$dep" ]; then
		to_add="$to_add $dep"
	fi
done

#Install deps
if [ "$to_add" != "" ]; then
	echo -e "The following packages need to be installed to proceed.\n$to_add"
	escalate apk add $to_add
fi

#Check for plugdev group
user_groups=$(groups | grep -o plugdev)

if [ "$user_groups" != "plugdev" ]; then
	echo "User is not in plugdev group, FEL mode will not work properly."
	shadow_groups=$(cat /etc/group | grep -o plugdev)

	if [ "$shadow_groups" != "plugdev" ]; then
		echo "plugdev group is missing, creating."
		escalate addgroup -S plugdev
	fi

	echo "Adding user to plugdev"
	user=$(whoami)
	escalate usermod -a -G plugdev $user

	echo "You must restart your session to continue."
	exit 1
fi

#Create udev rules
if [ ! -f /etc/udev/rules.d/99-chip.rules ]; then
	escalate touch /etc/udev/rules.d/99-chip.rules
	printf 'SUBSYSTEM=="usb", ATTRS{idVendor}=="1f3a", ATTRS{idProduct}=="efe8", GROUP="plugdev", MODE="0660" SYMLINK+="usb-chip"
SUBSYSTEM=="usb", ATTRS{idVendor}=="18d1", ATTRS{idProduct}=="1010", GROUP="plugdev", MODE="0660" SYMLINK+="usb-chip-fastboot"
SUBSYSTEM=="usb", ATTRS{idVendor}=="1f3a", ATTRS{idProduct}=="1010", GROUP="plugdev", MODE="0660" SYMLINK+="usb-chip-fastboot"
SUBSYSTEM=="usb", ATTRS{idVendor}=="067b", ATTRS{idProduct}=="2303", GROUP="plugdev", MODE="0660" SYMLINK+="usb-serial-adapter"' | escalate tee -a /etc/udev/rules.d/99-chip.rules

	escalate udevadm control --reload-rules
fi

# Create the rootfs from the minirootfs tarball
if [ ! -d $rootfs ]; then
	mkdir -p $rootfs
else
	if [ ! -d $backup ]; then
		mkdir -p $backup
	fi
	echo "Previous rootfs found, moving.."
	mv $rootfs $backup/rootfs-$(date +%s)
	mkdir -p $rootfs
fi

#Create cache dir if it doesn't exist
if [ ! -d $cache ]; then
	mkdir -p $cache
fi

#Fetch rootfs & kernel into cache dir
if [ ! -f $cache/$rootfs_tar ]; then
	wget $rootfs_url -O $cache/$rootfs_tar
fi

if [ ! -f $cache/$kernel_deb ]; then
	wget $kernel_url -O $cache/$kernel_deb
fi

if [ ! -f $cache/$arm_static ]; then
	wget $arm_static_url -O $cache/$arm_static
fi

#Extract Deb package for later use
cd $cache
ar x $kernel_deb
tar xf data.tar.xz
cd ..

#Extract minirootfs to rootfs dir
tar -xzf $cache/$rootfs_tar -C $rootfs
