#!/bin/ash
source common/utils.sh
source conf/variables.conf

#Try not to rm -rf the special filesystems.
check_mounts() {
	if [ $(mount | grep -o $rootfs/proc) ]; then
		   escalate umount $rootfs/proc
	fi

	if [ $(mount | grep -o $rootfs/sys) ]; then
		escalate umount $rootfs/sys
	fi
	
	if [ $(mount | grep -o $rootfs/dev) ]; then
		escalate umount $rootfs/dev
	fi
}

for opt in rootfs backups cache; do
	read -r -p "Would you like to purge $opt? [y/n] " input
	case $input in
		[Yy]* )
			if [ "$opt" == "rootfs" ]; then
				if [ -d $rootfs ]; then
					check_mounts
					escalate rm -rf $rootfs
				fi	   
			elif [ "$opt" == "backups" ]; then
				if [ -d $backup ]; then
					escalate rm -rf $backup
				fi
			elif [ "$opt" == "cache" ]; then
				if [ -d $cache ]; then
					rm -rf $cache
				fi
			fi;;
		[Nn]* ) ;;
	esac
done
