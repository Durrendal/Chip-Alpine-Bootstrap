#!/bin/ash
set -x
source conf/variables.conf

#Generate UBI configuration
if [ ! -f $cache/ubinize.cfg ]; then
	cat <<EOF > $cache/ubinize.cfg
[ubifs]
mode=ubi
vol_id=0
vol_type=dynamic
vol_name=rootfs
vol_alignment=1
vol_flags=autoresize
image=$cache/rootfs.ubifs
EOF
fi

#Generate Uboot Config
if [ ! -f $cache/flash.cmd ]; then
	cat <<EOF > $cache/flash.cmd
nand erase.chip
nand write.raw.noverify $spl_mem_addr 0x0 0x400000 # spl
nand write.raw.noverify $spl_mem_addr 0x400000 0x400000 # spl-backup
nand write $uboot_mem_addr 0x800000 0x400000 # uboot

env default -a
setenv bootargs $boot_args
setenv bootcmd $boot_cmd
setenv fel_booted 0

setenv stdin serial
setenv stdout serial
setenv stderr serial
saveenv
fastboot 0
mw \$${scriptaddr} 0x0
boot
EOF
fi

#Generate flash.scr
mkimage -A arm -T script -C none -n "flash" -d $cache/flash.cmd $cache/flash.scr

#These appear to work if you're using chip-mtd-tools
#mkfs.ubifs -d $rootfs \
#		   -m $nand_subpage_size \
#		   -e $nand_leb_size \
#		   -c $nand_maxleb_count \
#		   -o $cache/rootfs.ubi \
#		   -x lzo

#ubinize -o $cache/rootfs.ubi \
#		-p $nand_erase_block_size \
#		-m $nand_page_size \
#		-s $nand_subpage_size \
#		$cache/ubinize.cfg

#Create ubi file system
mkfs.ubifs -d $rootfs -o $cache/rootfs.ubifs -e 0x1f8000 -c 2000 -m 0x4000 -x lzo

#Build rootfs.ubi
ubinize -o $cache/rootfs.ubi -m 0x4000 -p 0x200000 -s 16384 $cache/ubinize.cfg

img2simg $cache/rootfs.ubi $cache/ubi.rootfs.sparse $nand_erase_block_size

#Build nand images
sunxi-nand-image-builder -c $nand_ecc_strength/$nand_ecc_step_size \
						 -p $nand_page_size \
						 -o $nand_oob_size \
						 -u $nand_usable_size \
						 -e $nand_erase_block_size \
						 -b -s $spl $pad1

i=0
repeat=$(($nand_erase_block_size / $nand_page_size / 64))
padding_size=$((64 - (`stat --printf="%s" $pad1` / ($nand_page_size + $nand_oob_size))))

while [ $i -lt $repeat ]; do
	dd if=/dev/urandom of=$pad2 bs=1024 count=$padding_size

	sunxi-nand-image-builder -c $nand_ecc_strength/$nand_ecc_step_size \
							 -p $nand_page_size \
							 -o $nand_oob_size \
							 -u $nand_usable_size \
							 -e $nand_erase_block_size \
							 -b -s $pad2 $pad3

	cat $pad1 $pad3 > $pad4

	if [ "$i" -eq "0" ]; then
		cat $pad4 > $spl
	else
		cat $pad4 >> $spl
	fi
	i=$((i+1))
done

##Flash the chip
#fastboot contiue
#fastboot erase UBI
#fastboot flash UBI $cache/rootfs.ubi.sparse

#sunxi-fel -v -p \
#		  spl $spl \
#		  write $spl_mem_addr $spl_ecc \
#		  write $uboot_mem_addr $cache/u-boot-dtb.bin \
#		  write $uboot_script_mem_addr $cache/flash.scr \
#		  exe $uboot_mem_addr

