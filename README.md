# What?

A collection of scripts to build an Alpine ubi image for a CHIP board, and flash it, from an Alpine system.

1_prepare.sh                       -> Ensures that necessary packages, directories, and resources exist.
2_rootfs.sh                        -> Builds an Alpine rootfs, and ubi image.
3_flash                            -> Flashes the CHIP with the UBI images built during the 2_rootfs.sh script step.

conf/variables.conf                -> Contains shared variables used in the above three scripts
conf/hynix_H27UCG8T2ETR-BC.conf    -> Configuration variables for the 8GB SK Hynix MLC NAND
conf/toshiba_TC58TEG5DCLTA00.conf  -> Configuration variables for the 4GB Toshiba MLC NAND
conf/toshiba_TC58NVG2S0HTA00.conf  -> Configuration variables for the 512MB Toshiba SLC NAND  <- [WARN] THIS IS UNTESTED

clean.sh                           -> Simple removal tool to cleanup the git repo

## Rootfs & Flash Process
Look at the model of CHIP you have, specifically looking at the NAND model number on the top of the chip. Then in conf/variables.conf modify the NAND configuration sources, such that only the configuration that matches the model on your chip is enabled.

```
#8GB SK Hynix MLC NAND
source hynix_H27UCG8T2ETR-BC.conf
#4GB Toshiba MLC NAND
#source toshiba_TC58TEG5DCLTA00.conf
#512MB Toshiba SLC NAND
#source toshiba_TC58NVG2S0HTA00.conf
```

By default this is the 8GB SK Hynix NAND, as that's the board I have the most of, and can provide the most extensive testing for. I do not own a Chip Pro board, the Toshiba SLC NAND configuration is my best attempt, but I cannot confirm it works.

Next place your chip into FEL mode using a jumper wire or thin paperclip.
![FEL mode](.images/fel-mode.webp)

Then connect the chip board to your computer using a micro usb cable.

Run the scripts in this order to pull deps, build and populate a rootfs enviroment, then flash the chip
```
./1_prepare.sh
./2_rootfs.sh
./3_flash.sh
```

## Custom Kernel Build Process

TODO: Add custom kernel compilation

## References

- https://wiki.alpinelinux.org/wiki/How_to_make_a_cross_architecture_chroot
- https://raw.githubusercontent.com/multiarch/qemu-user-static/master/containers/latest/register.sh
- https://github.com/Project-chip-crumbs/CHIP-tools
- https://github.com/knoopx/alpine-chip
- https://lore.kernel.org/all/20211216184448.27193-4-macroalpha82@gmail.com/T/
- https://ogdenslake.ca/2022/05/01/installing-a-custom-kernel-onto-the-chip/
- https://ogdenslake.ca/2023/02/04/installing-a-new-kernel-onto-the-chip-round-2/
- https://github.com/ntc-chip-revived/RTL8723BS
- https://github.com/ntc-chip-revived/linux
- https://github.com/ntc-chip-revived/CHIP-linux
- https://github.com/macromorgan/chip-debroot
- https://github.com/ntc-chip-revived/chip-os-pro/blob/master/build.sh
